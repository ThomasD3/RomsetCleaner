﻿namespace RomsetCleaner
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Text.RegularExpressions;

	public static class Program
	{
		public class Description
		{
			public string Filename;
			public List<string> Tags;
			public int Score;

			public Description()
			{
				Tags = new List<string>();
			}
		}

		public static Dictionary<string, List<Description>> Games = new Dictionary<string, List<Description>>();

		public static void Main(string[] Arguments)
		{
			// settings
			var Recursive		= false;
			var TestMode		= false;
			var Priorities		= new List<string>();
			var SourceFolder	= string.Empty;
			var FileFilter		= string.Empty;

			// check arguments
			if (Arguments.Length == 0) _Quit("Usage: RomsetCleaner [-r] [-t] <priorities> <rom path> <file filter>");

			// example: RomsetCleaner -r "!u|!e|!j|u|e|j|a|-h|-b" gbroms *.gb

			foreach (var Argument in Arguments)
			{
				if (Argument.StartsWith("-"))
				{
					switch (Argument.ToLower())
					{
						case "-r":	Recursive = true; break;
						case "-t":	TestMode = true; break;
						default:
							_Quit($"Unknown flag {Argument}");
							break;
					}
				}
				else
				{
					if (Priorities.Count == 0)						Priorities = Argument.ToLower().Trim('"').Trim('\'').Split('|').ToList();
					else if (string.IsNullOrEmpty(SourceFolder))	SourceFolder = Path.GetFullPath(Argument) + Path.DirectorySeparatorChar;
					else if (string.IsNullOrEmpty(FileFilter))		FileFilter = Argument;
				}
			}

			// check if we have all parameters
			if (string.IsNullOrEmpty(FileFilter)) _Quit("Invalid command line parameters. Example: RomsetCleaner -r \"!u|!e|!j|u|e|j|a|-o|-h|-b\" gbroms *.gb");

			// verify the that the source directory is valid
			if (!Directory.Exists(SourceFolder)) _Quit($"Source folder {SourceFolder} doesn't exist");

			// define the regex we're going to use
			var ParenthesisGroupRegex	= new Regex(@"\ ?[\[\(].*?[\]\)]");
			var ParenthesisRegex		= new Regex(@"[\ \(\)\[\]]");
	
			// scan the roms
			Console.WriteLine("Scanning roms...");

			// get all files and sort them
			foreach (var F in Directory.EnumerateFiles(SourceFolder, FileFilter, Recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly))
			{
				// make a key out of the simplified name
				var Key = ParenthesisGroupRegex.Replace(F.ToLower(), string.Empty).Trim();

				// get the tags
				var Matches = ParenthesisGroupRegex.Matches(F.ToLower());
				var D = new Description { Filename = F };
				foreach (Match Match in Matches)
				{
					var CleanedMatches = ParenthesisRegex.Replace(Match.Value, string.Empty);
					var MatchesList = CleanedMatches.Split(',').ToList();

					var Disk = MatchesList.FirstOrDefault(_ => _.StartsWith("disc") || _.StartsWith("disk"));
					if (!string.IsNullOrEmpty(Disk)) Key += Disk;
					MatchesList.RemoveAll(_ => _.StartsWith("disc") || _.StartsWith("disk"));

					D.Tags.AddRange(MatchesList);
				}

				// calculate the score
				D.Score = int.MaxValue / 2;
				for (var i = 0; i < Priorities.Count; i++)
				{
					var S = Priorities[i].StartsWith("-") ? 1 : 0;
					var AllMatch = Priorities[i].Substring(S).All(C => D.Tags.Any(_ => _.StartsWith(C.ToString())));
					if (AllMatch)
					{
						if (S == 1) D.Score = int.MaxValue;
						else if (i < D.Score) D.Score = i;
					}
				}

				// create the key if it doesn't exist
				if (!Games.ContainsKey(Key)) Games[Key] = new List<Description>();

				// add the descriptions
				Games[Key].Add(D);
			}
	
			// create the removed folder
			var RemovedFolder = SourceFolder + Path.DirectorySeparatorChar + "removed" + Path.DirectorySeparatorChar;
			if (!TestMode && !Directory.Exists(RemovedFolder)) Directory.CreateDirectory(RemovedFolder);

			// move the games out
			var FileCounter = 0;
			var RemovedCounter = 0;
			foreach (var Game in Games)
			{
				// sort the files by score (lower is better)
				var SortedFiles = Game.Value.OrderBy(_ => _.Score);

				// go through all the files, the first one is the one with the highest score
				foreach (var Description in SortedFiles)
				{
					if (Description.Filename.Equals(SortedFiles.First().Filename))
					{
						Console.WriteLine($"+ {Description.Filename} (score {Description.Score})");
					}
					else
					{
						Console.WriteLine($"- {Description.Filename} (score {Description.Score})");
						if (!TestMode) File.Move(Description.Filename, RemovedFolder + Path.GetFileName(Description.Filename));
						RemovedCounter++;
					}

					FileCounter++;
				}

				Console.WriteLine();
			}

			Console.WriteLine($"{FileCounter} files found, {Games.Count} unique games, {RemovedCounter} files removed");
			if (TestMode) Console.WriteLine("Was run in test mode, no files have been changed");

			void _Quit(string Message)
			{
				Console.WriteLine(Message);
				Environment.Exit(-1);
			}
		}
	}
}
