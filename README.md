# RomsetCleaner

This tool was built after my friend introduced me to RecalBox.

I wanted to have a single version of each game and I didn't care about having the Japanese version or 3rd version of the Brazilian bootleg of a specific game.

I wrote it for my personal use, added some functionalities to make it usable by others, however, it hasn't been tested anywhere else, so use at your own risk :)

The tool is quite simple to use:

RomsetCleaner [-r] [-t] <priorities> <rom path> <file filter>

 -r : recurse through the folders
 
 -t : test run, display what would have happened
 
 <priorities> : this is the order of priority for the tags
 
 <rom path> : the directory with the roms for a specific system
 
 <file filter> : the filter to pick the right files

For example:

RomsetCleaner -r \"!u|!e|!j|u|e|j|a|-o|-h|-b\" gbroms *.gb

will recursively scan the 'gbroms' folder for *.gb files.

The priorities are a bunch of tags separated by the pipe character ('|').
The roms get grouped by game, then tags get analyzed and the leftmost tags get the highest priority.
Priorities starting with a '-' are avoided as much as possible.
You may need to wrap the priorities in quotes because of the command line interpreter.

Let's assume you have the following priorities: !u|!e|!j|u|e|j|a|-o|-h|-b

and the following roms:

 Alien 3 (J) [!].gb
 Alien 3 (E) [!].gb
 Alien 3 (U).gb

All files will be grouped as the game "Alien 3" and "Alien 3 (E) [!].gb" would be selected.

 
Files are moved into a 'removed' folder and no file is ever deleted.

Since the binary file is only 10kb, I have included it. You will need the .NET 4.7 framework installed to run it.
